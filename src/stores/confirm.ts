import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useProductStore } from "./product";
import { useEmployeeStore } from "./employee";

export const useConfirmStore = defineStore("confirm", () => {
  const isShow = ref(false);
  const confirm = ref("");
  const product = useProductStore();
  const employee = useEmployeeStore();
  const Id = ref<number>();

  function showError(text: string) {
    confirm.value = text;
    isShow.value = true;
  }

  function acceptConfirm(id: number) {
    if (Id.value !== -1) {
      product.deleteProduct(id);
      employee.deleteEmployee(id);
      Id.value = -1;
      isShow.value = false;
    }
  }

  function cancelConfirm(id: number) {
    isShow.value = true;
    Id.value = id;
  }
  return { isShow, confirm, showError, acceptConfirm, cancelConfirm, Id };
});
